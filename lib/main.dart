import 'package:flutter/material.dart';

import 'Constants.dart';
import 'launch/BlocRXApplication.dart';

void main() {
  Constants.setArchitecture(Architecture.BLOC_RX);
  runApp(BlocRXApplication());
}