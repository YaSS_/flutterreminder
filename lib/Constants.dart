enum Architecture { SCOPED_MODEL, BLOC, BLOC_RX, REDUX }

class Constants {
  static Architecture _config;

  static void setArchitecture(Architecture type) {
    switch (type) {
      case Architecture.BLOC:
        _config = Architecture.BLOC;
        break;
      case Architecture.BLOC_RX:
        _config = Architecture.BLOC_RX;
        break;
      case Architecture.REDUX:
        _config = Architecture.REDUX;
        break;
      case Architecture.SCOPED_MODEL:
        _config = Architecture.SCOPED_MODEL;
        break;
    }
  }

  static get config => _config;
}
