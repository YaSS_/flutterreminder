import 'dart:async';

import 'package:reminders/data/Remind.dart';

import 'package:reminders/utils/DateUtils.dart';
import 'Bloc.dart';

class ReminderBloc extends BlocBase {
  final List<Reminder> _reminders = [Reminder("Read about Skia", "Сегодня 17:30"),Reminder("Developing", "Сегодня 20:35")];

  final StreamController _streamController =
      StreamController<DataState>.broadcast();

  Stream<DataState> get data => _streamController.stream;

  void loadReminders() {
    _streamController.sink.add(LoadingState());
    Timer(const Duration(seconds: 6), () {
      _streamController.sink.add(LoadedState(_reminders));
    });
  }

  void addReminder(String title) {
    final reminder = Reminder(title, getDateForReminder());
    _reminders.add(reminder);
    _streamController.sink.add(LoadedState(_reminders));
  }

  void deleteReminder(Reminder reminder) {
    _reminders.remove(reminder);
    _streamController.sink.add(LoadedState(_reminders));
  }

  @override
  void dispose() {
    _streamController.close();
  }
}

abstract class DataState {}

class LoadingState extends DataState {}

class ErrorLoadingState extends DataState {}

class LoadedState extends DataState {
  final List<Reminder> reminders;

  LoadedState(this.reminders);
}
