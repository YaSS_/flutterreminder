import 'package:flutter/material.dart';

Type _getType<B>() => B;

abstract class BlocBase {
  void dispose();
}

class BlocProvider<B extends BlocBase> extends StatefulWidget {
  final Widget child;
  final B Function(B bloc) builder;

  BlocProvider({Key key, @required this.builder, @required this.child})
      : super(key: key);

  @override
  BlocProviderState<B> createState() => BlocProviderState<B>();
}

class BlocProviderState<B extends BlocBase> extends State<BlocProvider<B>> {
  B bloc;

  @override
  void initState() {
    if (widget.builder != null) {
      bloc = widget.builder(bloc);
      print('bloc != null : ${bloc != null}');
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Provider(
      child: widget.child,
      bloc: bloc,
    );
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }
}

class Provider<B extends BlocBase> extends InheritedWidget {
  final B bloc;

  Provider({Key key, Widget child, this.bloc}) : super(key: key, child: child) {
    print('Provider().bloc != null ${bloc != null}');
  }

  @override
  bool updateShouldNotify(Provider<B> oldWidget) {
    return oldWidget.bloc != bloc;
  }

  static B of<B extends BlocBase>(BuildContext ctx) {
    final type = _getType<Provider<B>>();
    final Provider<B> provider =
        ctx.ancestorInheritedElementForWidgetOfExactType(type)?.widget;
    print('provider != null ${provider != null}');
    print('trying to return bloc');
    return provider?.bloc;
  }
}
