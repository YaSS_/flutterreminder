import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:reminders/redux/ApplicationState.dart';

import 'Constants.dart';
import 'data/Remind.dart';
import 'launch/ReduxApplication.dart';

void main() {
  Constants.setArchitecture(Architecture.REDUX);
  final Store<ApplicationState> appStore = Store<ApplicationState>(reminderReducer,
      initialState: ApplicationState([], ReduxActions.LOADING_REMINDER), middleware: [mockMiddleware()]);
  runApp(ReduxApplication(Key(appStore.hashCode.toString()), appStore));
}
