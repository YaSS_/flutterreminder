import 'package:flutter/material.dart';
import 'package:reminders/data/Remind.dart';
import 'package:reminders/scopedModel/RemindModel.dart';
import 'package:scoped_model/scoped_model.dart';

import '../Constants.dart';
import 'package:reminders/utils/UiUtils.dart';

class ScopedModelApplication extends StatelessWidget {
  final RemindModel _remindModel = RemindModel();
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: ScopedModel<RemindModel>(
      model: _remindModel,
      child: Scaffold(
        appBar: AppBar(
          title: Text(Constants.config.toString()),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: ScopedModelList(),
            ),
            getTextField(_controller, () {
              _addNewReminder();
            })
          ],
        ),
      ),
    ));
  }

  void _addNewReminder() {
    _remindModel.addReminder(_controller.text);
    _controller.clear();
  }
}

class ScopedModelList extends StatelessWidget {
  @override
  Widget build(BuildContext context) =>
      ScopedModelDescendant<RemindModel>(builder: (ctx, child, model) {
        return model.isLoading
            ? buildProgressWidget()
            : model.reminderList.length == 0
                ? buildEmptyScreen()
                : getListView(model.reminderList, (Reminder reminder) {
                    model.removeReminder(reminder);
                  });
      });
}
