import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reminders/blocrx/ReminderBlocRX.dart';
import 'package:reminders/blocrx/ReminderEvents.dart';
import 'package:reminders/blocrx/ReminderStates.dart';

import '../Constants.dart';
import 'package:reminders/utils/UiUtils.dart';

class BlocRXApplication extends StatelessWidget {
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
//    final ReminderBloc bloc = BlocProvider.of<ReminderBloc>(context);
    return MaterialApp(
      theme: ThemeData.fallback(),
      home: Scaffold(
          appBar: AppBar(
            title: Text(Constants.config.toString()),
          ),
          body: BlocProvider<ReminderBlocRX>(
            builder: (ctx) => ReminderBlocRX()..dispatch(LoadReminders()),
            child: BlocBuilder<ReminderBlocRX, ReminderStates>(
              builder: (ctx, state) {
                print('state ---- $state');
                if (state is LoadingState) {
                  return buildProgressWidget();
                } else if (state is LoadedState) {
                  var reminders = state.reminders;
                  return Column(
                    children: <Widget>[
                      Expanded(
                        child: reminders.length == 0
                            ? buildEmptyScreen()
                            : getListView(reminders, (reminder) {
                                BlocProvider.of<ReminderBlocRX>(ctx)
                                    .dispatch(DeleteReminder(reminder));
                              }),
                      ),
                      getTextField(_controller, () {
                        _addNewReminder(BlocProvider.of<ReminderBlocRX>(ctx));
                      })
                    ],
                  );
                } else {
                  return Center(
                    child: Text("Error loading"),
                  );
                }
              },
            ),
          )),
    );
  }

  _addNewReminder(ReminderBlocRX bloc) {
    bloc.dispatch(AddReminder(_controller.text));
    _controller.clear();
  }
}
