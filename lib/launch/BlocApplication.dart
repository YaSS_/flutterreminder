import 'package:flutter/material.dart';
import 'package:reminders/bloc/Bloc.dart';
import 'package:reminders/bloc/ReminderBloc.dart';

import '../Constants.dart';
import 'package:reminders/utils/UiUtils.dart';

class BlocApplication extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => ReminderBlocScreenState();
}

class ReminderBlocScreenState extends State<BlocApplication> with WidgetsBindingObserver{
  TextEditingController _controller = TextEditingController();

  AppLifecycleState _lifecycleState ;

  @override
  void initState() {
    super.initState();
    Provider.of<ReminderBloc>(context).loadReminders();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    Provider.of<ReminderBloc>(context).dispose();
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final bloc = Provider.of<ReminderBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(Constants.config.toString()),
      ),
      body: StreamBuilder<DataState>(
          initialData: LoadingState(),
          stream: bloc.data,
          builder: (context, snapshot) {
            return Column(
              children: <Widget>[
                Expanded(child: _buildByState(snapshot, bloc)),
                getTextField(_controller, () {
                  _addNewReminder(bloc);
                })
              ],
            );
          }),
    );
  }

  Widget _buildByState(AsyncSnapshot snapshot, ReminderBloc bloc) {
    if (snapshot.data is LoadingState || snapshot.data == null) {
      return buildProgressWidget();
    } else if (snapshot.data is LoadedState) {
      var reminders = (snapshot.data as LoadedState).reminders;
      return Column(
        children: <Widget>[
          Expanded(
            child: reminders.length == 0
                ? buildEmptyScreen()
                : getListView(reminders, (reminder) {
                    bloc.deleteReminder(reminder);
                  }),
          )
        ],
      );
    } else {
      return buildEmptyScreen();
    }
  }

  _addNewReminder(ReminderBloc bloc) {
    if (_controller.text.isNotEmpty) {
      bloc.addReminder(_controller.text);
      _controller.clear();
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed){
      Provider.of<ReminderBloc>(context).loadReminders();
    }
    super.didChangeAppLifecycleState(state);
  }
}
