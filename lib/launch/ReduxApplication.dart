import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:reminders/data/Remind.dart';
import 'package:reminders/redux/ApplicationState.dart';

import '../Constants.dart';
import 'package:reminders/utils/DateUtils.dart';
import 'package:reminders/utils/UiUtils.dart';

class ReduxApplication extends StatelessWidget {
  final TextEditingController _controller = TextEditingController();
  final Store<ApplicationState> _store;

  ReduxApplication(Key key, this._store) : super(key: key) {
    _loadReminders();
  }

  @override
  Widget build(BuildContext context) => StoreProvider<ApplicationState>(
        store: _store,
        child: MaterialApp(
          theme: ThemeData.dark(),
          home: Scaffold(
            appBar: AppBar(title: Text(Constants.config.toString())),
            body: Column(
              children: <Widget>[
                Expanded(
                  child: StoreConnector<ApplicationState, ApplicationState>(
                    converter: (Store<ApplicationState> store) => store.state,
                    builder: (ctx, ApplicationState state) {
                      return state.action == ReduxActions.LOADING_REMINDER
                          ? buildProgressWidget()
                          : state.reminders.length == 0
                              ? buildEmptyScreen()
                              : getListView(state.reminders,
                                  (Reminder reminder) {
                                  _removeReminder(reminder);
                                });
                    },
                  ),
                ),
                getTextField(_controller, () {
                  _addNewReminder();
                })
              ],
            ),
          ),
        ),
      );

  void _loadReminders() {
    _store.dispatch(ReminderAction(null, ReduxActions.LOADING_REMINDER));
  }

  void _removeReminder(Reminder reminder) {
    _store.dispatch(ReminderAction(reminder, ReduxActions.REMOVE_REMINDER));
  }

  void _addNewReminder() {
    _store.dispatch(ReminderAction(
        Reminder(_controller.text, getDateForReminder()),
        ReduxActions.ADD_REMINDER));
    _controller.clear();
  }
}
