import 'dart:math';

import 'package:flutter/material.dart';

class CircularLoaderWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => CircularLoaderWidgetState();
}

class CircularLoaderWidgetState extends State<CircularLoaderWidget> with SingleTickerProviderStateMixin {
  final initialRadius = 20.0;
  double radius = 0.0;

  AnimationController _controller;
  Animation<double> _animation_rotation;
  Animation<double> _animation_radius_in;
  Animation<double> _animation_radius_out;

  @override
  void initState() {
    super.initState();
    _controller =
        AnimationController(vsync: this, duration: Duration(seconds: 4));

    _animation_rotation = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: _controller,
            curve: Interval(0.0, 1.0, curve: Curves.linear)));

    _animation_radius_in = Tween<double>(begin: 1.0, end: 0.0).animate(
        CurvedAnimation(
            parent: _controller,
            curve: Interval(0.75, 1.0, curve: Curves.elasticIn)));
    _animation_radius_out = Tween<double>(begin: 0.0, end: 1.0).animate(
        CurvedAnimation(
            parent: _controller,
            curve: Interval(0.0, 0.25, curve: Curves.elasticOut)));

    _controller.addListener(() {
      setState(() {
        if (_controller.value >= 0.75 && _controller.value <= 1.0) {
          radius = _animation_radius_in.value * initialRadius;
        } else if (_controller.value >= 0.0 && _controller.value <= 0.25) {
          radius = _animation_radius_out.value * initialRadius;
        }
      });
    });

    _controller.repeat();
  }

  @override
  Widget build(BuildContext context) => Container(
        width: 100.0,
        height: 100.0,
        child: Center(
          child: RotationTransition(
            turns: _animation_rotation,
            child: Stack(
              children: <Widget>[
                Center(
                  child: Container(
                    height: 20,
                    width: 20,
                    child: Image.asset(
                      "assets/googlelogo2.png",
                      height: 20,
                      width: 20,
                    ),
                    decoration: BoxDecoration(shape: BoxShape.circle),
                  ),
                ),
                DotPosition(
                  radius: radius,
                  position: 1,
                  body: _buildDot(Colors.red),
                ),
                DotPosition(
                  radius: radius,
                  position: 2,
                  body: _buildDot(Colors.green),
                ),
                DotPosition(
                  radius: radius,
                  position: 3,
                  body: _buildDot(Colors.purple),
                ),
                DotPosition(
                  radius: radius,
                  position: 4,
                  body: _buildDot(Colors.yellowAccent),
                ),
                DotPosition(
                  radius: radius,
                  position: 5,
                  body: _buildDot(Colors.blue),
                ),
                DotPosition(
                  radius: radius,
                  position: 6,
                  body: _buildDot(Colors.orange),
                ),
                DotPosition(
                  radius: radius,
                  position: 7,
                  body: _buildDot(Colors.pinkAccent),
                ),
                DotPosition(
                  radius: radius,
                  position: 8,
                  body: _buildDot(Colors.tealAccent),
                ),
              ],
            ),
          ),
        ),
      );

  Dot _buildDot(Color color) => Dot(
        color: color,
        radius: 6.5,
      );
}

class DotPosition extends StatelessWidget {
  final Widget body;
  final radius;
  int _position;

  DotPosition({this.body, int position, this.radius}) {
    _position = position == 0 ? 1 : position;
  }

  @override
  Widget build(BuildContext context) => Transform.translate(
        offset: Offset(
            radius * cos(_position * pi / 4), radius * sin(_position * pi / 4)),
        child: body,
      );
}

class Dot extends StatelessWidget {
  final double radius;
  final Color color;

  Dot({this.radius, this.color});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: radius,
        height: radius,
        decoration: BoxDecoration(color: color, shape: BoxShape.circle),
      ),
    );
  }
}
