import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:reminders/data/Remind.dart';
import 'package:scoped_model/scoped_model.dart';

import 'package:reminders/utils/DateUtils.dart';

class RemindModel extends Model {
  List<Reminder> _reminders = [];
  bool _isLoading = true;

  RemindModel() {
    _mockLoadingStatus();
  }

  _mockLoadingStatus() async {
    Timer(const Duration(seconds: 6), () {
      _isLoading = false;
      notifyListeners();
    });
  }

  List<Reminder> get reminderList => _reminders;

  bool get isLoading => _isLoading;

  void addReminder(String title) {
    var remind = Reminder(title, getDateForReminder());
    _reminders.add(remind);

    notifyListeners();
  }

  void removeReminder(Reminder reminder){
    _reminders.remove(reminder);

    notifyListeners();
  }

  static RemindModel of(BuildContext ctx) => ScopedModel.of<RemindModel>(ctx);
}