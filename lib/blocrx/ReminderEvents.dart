import 'package:meta/meta.dart';
import 'package:reminders/data/Remind.dart';

@immutable
abstract class ReminderEvents {}

class LoadReminders extends ReminderEvents {
  @override
  String toString() => "Load reminder list";
}

class AddReminder extends ReminderEvents {
  final String reminderTitle;

  AddReminder(this.reminderTitle);

  @override
  String toString() => "Add reminder to list";
}

class DeleteReminder extends ReminderEvents {
  final Reminder reminder;

  DeleteReminder(this.reminder);

  @override
  String toString() => "Delete reminder into list";
}
