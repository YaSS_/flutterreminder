import 'package:bloc/bloc.dart';
import 'package:reminders/data/Remind.dart';

import 'package:reminders/utils/DateUtils.dart';
import 'ReminderEvents.dart';
import 'ReminderStates.dart';

class ReminderBlocRX extends Bloc<ReminderEvents, ReminderStates> {
  List<Reminder> _reminders = [];

  @override
  ReminderStates get initialState => LoadingState();

  @override
  Stream<ReminderStates> mapEventToState(ReminderEvents event) async* {
    if (event is LoadReminders) {
      yield* _mapLoadRemindersState();
    } else if (event is AddReminder) {
      yield* _mapAddReminderState(event);
    } else if (event is DeleteReminder) {
      yield* _mapDeleteReminderState(event);
    }
  }

  Stream<ReminderStates> _mapLoadRemindersState() async* {
    Future.delayed(
        const Duration(seconds: 7)); //only for showing loading status
    yield LoadedState(reminders: _reminders);
  }

  Stream<ReminderStates> _mapAddReminderState(AddReminder event) async* {
    if (currentState is LoadedState) {
      final reminder = Reminder(event.reminderTitle, getDateForReminder());
      final List<Reminder> updatedReminders =
          List.from((currentState as LoadedState).reminders)..add(reminder);

      yield LoadedState(reminders: updatedReminders);

      _updateReminders(updatedReminders);
    }
  }

  Stream<ReminderStates> _mapDeleteReminderState(DeleteReminder event) async* {
    if (currentState is LoadedState) {
      final updateReminders = (currentState as LoadedState)
          .reminders
          .where((remind) => remind != event.reminder)
          .toList();

      yield LoadedState(reminders: updateReminders);

      _updateReminders(updateReminders);
    }
  }

  _updateReminders(List<Reminder> reminders) {
    _reminders = reminders;
  }
}
