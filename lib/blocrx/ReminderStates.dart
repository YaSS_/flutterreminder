import 'package:meta/meta.dart';
import 'package:reminders/data/Remind.dart';

@immutable
abstract class ReminderStates {}

class LoadingState extends ReminderStates {
  LoadingState(){toString();}

  @override
  String toString() => "Reminders loading";
}

class LoadedState extends ReminderStates {
  final List<Reminder> reminders;

  LoadedState({this.reminders = const []});

  @override
  String toString() => "Reminders loaded";
}

class NotLoadedState extends ReminderStates {
  @override
  String toString() => "Reminders not loaded";
}
