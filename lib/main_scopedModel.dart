import 'package:flutter/material.dart';
import 'Constants.dart';
import 'launch/ScopedModelApplication.dart';

void main() {
  Constants.setArchitecture(Architecture.SCOPED_MODEL);
  runApp(ScopedModelApplication());
}
