import 'package:intl/intl.dart';

final format = DateFormat("dd-MM-yyyy HH:MM");

String getDateForReminder(){
  return format.format(DateTime.now());
}