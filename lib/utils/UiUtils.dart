import 'package:flutter/material.dart';
import 'package:reminders/data/Remind.dart';
import 'package:reminders/widget/Loader.dart';

Widget getTextField(TextEditingController controller, Function func) =>
    SafeArea(
        bottom: true,
        child: Padding(
            padding: EdgeInsets.fromLTRB(7, 0, 7, 0),
            child: TextField(
              decoration: InputDecoration(
                suffixIcon: IconButton(
                  icon: Icon(Icons.add),
                  onPressed: () {
                    func();
                  },
                ),
                hintText: "enter task",
              ),
              controller: controller,
            )));

ListView getListView(List<Reminder> list, Function removeItem) {
  ScrollController controller = ScrollController();
  //todo add listener to controller for scroll to end
  return ListView.builder(
      controller: controller,
      itemCount: list.length,
      itemBuilder: (context, int index) {
        return Dismissible(
          direction: DismissDirection.endToStart,
          key: Key(list[index].hashCode.toString()),
          onDismissed: (direction) {
            if (direction == DismissDirection.endToStart) {
              removeItem(list[index]);
            }
          },
          background: Padding(
            padding: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
            child: Container(
              color: Colors.red,
              child: Padding(
                padding: EdgeInsets.only(right: 10.0),
                child: Icon(
                  Icons.delete,
                  color: Colors.white,
                ),
              ),
              alignment: Alignment.centerRight,
            ),
          ),
          child: buildRemindItem(list[index]),
        );
      });
}

List<Widget> buildReminderListView(List<Reminder> list) =>
    list.map((remind) => buildRemindItem(remind)).toList();

Widget buildProgressWidget() {
  return Center(child:  CircularLoaderWidget());
}

Widget buildRemindItem(Reminder reminder) => Card(
    elevation: 4,
    child: ListTile(
      title: Text(reminder.title),
      subtitle: Text(reminder.time ?? ""),
    ));

Widget buildEmptyScreen() => Center(
      child: Text("No results"),
    );
