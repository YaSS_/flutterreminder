import 'dart:async';

import 'package:reminders/data/Remind.dart';
import 'package:redux/redux.dart';

enum ReduxActions { ADD_REMINDER, REMOVE_REMINDER, LOADING_REMINDER }

class ReminderAction {
  final Reminder reminder;
  final ReduxActions action;

  ReminderAction(this.reminder, this.action);
}

ApplicationState reminderReducer(ApplicationState appState, dynamic action) {
  switch (action.action) {
    case ReduxActions.ADD_REMINDER:
      if (action.reminder.title.toString().isEmpty) {
        return appState;
      } else {
        final reminders = appState.reminders;
        reminders.add(action.reminder);
        return ApplicationState(reminders, action.action);
      }
      break;
    case ReduxActions.REMOVE_REMINDER:
      final reminders = appState.reminders;
      reminders.remove(action.reminder);
      return ApplicationState(reminders, action.action);
      break;
    default:
      return appState;
      break;
  }
}

Middleware<ApplicationState> mockMiddleware() {
  return (Store<ApplicationState> store, action, NextDispatcher next) async {
    if (action.action == ReduxActions.LOADING_REMINDER) {
      Future.delayed(const Duration(seconds: 7)).then((s) {
        store.dispatch(ReminderAction(
            Reminder("New test reminder", "22:36"), ReduxActions.ADD_REMINDER));
      });
      next(action);
    } else {
      next(action);
    }
  };
}

class ApplicationState {
  final List<Reminder> reminders;
  ReduxActions action;

  ApplicationState(this.reminders, this.action);
}
