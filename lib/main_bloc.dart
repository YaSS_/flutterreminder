import 'package:flutter/material.dart';

import 'Constants.dart';
import 'bloc/Bloc.dart';
import 'bloc/ReminderBloc.dart';
import 'launch/BlocApplication.dart';

void main() {
  Constants.setArchitecture(Architecture.BLOC);
  runApp(MaterialApp(builder: (context, child) {
    return BlocProvider<ReminderBloc>(
      builder: (bloc) => bloc ?? ReminderBloc(),
      child: BlocApplication(),
    );
  }));
}
